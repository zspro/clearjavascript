
var formValidator = (function(){
	
	var DOMElements = null;
	var index = 0;
	var msgError = document.getElementById("msgError").classList;
	var msgSuccess = document.getElementById("msgSuccess").classList;
	var informTable = document.querySelector("#data-table").classList;
	var btnHide = document.querySelector("#data-table");
	var formArr =[];
	
	function showErrorMsg(){  //Собщение, неправильно заполнена форма
		msgError.add('bg-danger');
		msgError.remove('hide');
		if(!msgSuccess.contains('hide')){
			msgSuccess.add('hide');
		};
	}
	
	function showSuccessMsg(){ //Сообщение, правильно заполнена форма
		msgSuccess.add('bg-success');
		msgSuccess.remove('hide');
		if(!msgError.contains('hide')){
			msgError.add('hide');
		};
	}
	
	function inspectionMail(mail) { // Проверка почты
		var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;

		var valid = re.test(mail);
		if (valid) {
			return true
		} else{
			return false
		}
	}

	function showHidePassword(inputPassword) { //Скрыть/показать пароль в форме
		event.preventDefault();
		var element = document.getElementById("inputPassword");
		if (element.type == 'password') {
			var inp = document.createElement("input");
			inp.id = "inputPassword";
			inp.type = "text";
			inp.value = element.value;
			inp.classList = element.classList;
			element.parentNode.replaceChild(inp, element);
		}
		else {
			var inp = document.createElement("input");
			inp.id = "inputPassword";
			inp.type = "password";
			inp.value = element.value;
			inp.classList = element.classList;
			element.parentNode.replaceChild(inp, element);
		}
	}

	function showHidePasswordTable(event) { //Скрыть/Показать пароль в таблице
		var tr = event.target.closest("tr");
		var index = tr.getAttribute("data-index");
		var passwordCell = tr.getElementsByClassName("password-cell")[0];
		var passwordButton = tr.getElementsByClassName("show-password")[0];
		var passwordStatus = passwordButton.getAttribute("data-status");
		if(passwordStatus == 'hide'){
			passwordCell.innerHTML = formArr[index].password;
			passwordButton.innerHTML = "Спрятать";
			passwordButton.setAttribute("data-status", "show");
		}else if(passwordStatus == "show"){
			passwordCell.innerHTML = formArr[index].passwordStars;
			passwordButton.innerHTML = "Показать";
			passwordButton.setAttribute("data-status", "hide");
		}

	}

	function toggleTableVisibility() {
		if(informTable.contains('hide')){
			informTable.remove('hide');
		}else if(!informTable.contains('hide')){
			informTable.add('hide');
			clearForm();
		}
	}

	function clearForm() {
		DOMElements.name.value = "";
		DOMElements.email.value = "";
		DOMElements.password.value = "";
	}

	function convertCharactersToAsteriks(string) {
		return string.replace(/[\w \W]/g, '*');
	}

	function createArrayItem() {
		return {
			name: DOMElements.name.value,
			email: DOMElements.email.value,
			password:DOMElements.password.value,
			passwordStars:convertCharactersToAsteriks(DOMElements.password.value)
		};
	}
	
	function addNewTableLine() {  //Заполнение таблицы формы данными
		var newItem = createArrayItem();
		formArr.push(newItem);

		var row = `<tr data-index="${index}">\
						<th scope="row">${index + 1}</th>\
						<td>${DOMElements.name.value}</td>\
                		<td>${DOMElements.email.value}</td>\
						<td class="password-cell">${convertCharactersToAsteriks(DOMElements.password.value)}
						</td>
						<td><button class="show-password" data-status="hide">Показать</button>\
						</td>\
					</tr>`;
			
		DOMElements.tableContent.innerHTML += row;
		toggleTableVisibility();// Показывает таблицу с данными
		index++;
	}
	
	function validate (event) { //Проверка формы
		if(DOMElements.name.value
			&& inspectionMail(DOMElements.email.value)
			&& DOMElements.password.value) {
				showSuccessMsg(); //Сообщение, правильно заполнена форма
				addNewTableLine();
				event.preventDefault();
			
		} else {
			event.preventDefault();
			showErrorMsg();
		}
	}
	
	function initListeners() {
		DOMElements.submitBtn.addEventListener("click", validate);
		DOMElements.resetBtn.addEventListener("click", toggleTableVisibility);
		DOMElements.hideOpenBtn.addEventListener("click", showHidePassword);
		DOMElements.tableContent.addEventListener("click", showHidePasswordTable);
	}

	
	return {
		setFormData : function(form){	
			DOMElements = form;
		},
		initValidator: function(form){
			initListeners();
		}
	}
	
}());

formValidator.setFormData({
	name 	: document.querySelector("#inputName"),
	email 	: document.querySelector("#inputEmail"),
	password : document.querySelector("#inputPassword"),
	tableContent : document.querySelector("#table-content"),
	submitBtn: document.querySelector("#submit"),
	resetBtn: document.querySelector("#reset"),
	hideOpenBtn: document.querySelector("#btn_hide-open")
});

formValidator.initValidator();