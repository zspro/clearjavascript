
(function(){
    
	var model = app.model;
    var Gallery = app.Gallery;
    
	function controller() {
        var gallery = null;
        //var eventHoder = $( model );
             
        function save() {
            gallery.saveDefer.then(function(data){
                model.saveData(data);    
            });
        }
        
        function update() {
            gallery.eventHolder.on( gallery.updateEventName, function (event, params){
                model.updateData(params);
            });
        }
        
        function bindEvents(){
            save();  
            update();
        }
        
        function initGallery(data){
            gallery = new Gallery(data); 
            bindEvents();
        }
        
        function init() {
            model.getData().then(initGallery);    
        }
        init();
    }
    
    
    controller();
    
    
    
    /*=========
    var data = model.getData();
    var gallery = new Gallery(data);
    
    var form = gallery.getForm();
    model.save(form);
    =========*/
    
}())
