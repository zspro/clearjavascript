
var formValidator = (function(){
	
	var DOMElements = null;
	
	function showErrorMsg(){}
	function hideErrorMsg(){}
	
	function showSuccessMsg(){}
	function hideSuccessMsg(){}
	
	function showTable(){}
	function hideTable(){}
	
	
	function addNewTableLine() {
		var row;
			
		DOMElements.tableContent.innerHTML += row
		showSuccessMsg();
		showTable();
	}
	
	function validate () {
		if(DOMElements.name.value 
			&& DOMElements.email.value 
			&& DOMElements.password.value) {			
				showTable();
			
		} else {
			showErrorMsg();
		}
	}
	
	function initListeners() {
		DOMElements.submitBtn.addEventListener("click", validate);	
	}
	
	return {
		setFormData : function(form){	
			DOMElements = form;
		},
		initValidator: function(form){
			initListeners();
		}
	}
	
}())

formValidator.setFormData({
	name 	: document.querySelector("inputName"),
	email 	: document.querySelector("inputEmail"),
	password : document.querySelector("inputPassword"),
	tableContent : document.querySelector("table-content"),
	submitBtn: document.querySelector(""),
	resetBtn: document.querySelector("")
})
formValidator.initValidator();