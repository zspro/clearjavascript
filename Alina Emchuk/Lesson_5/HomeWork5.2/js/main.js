var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var resultGame = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}
function getNameById(playerResultNumber) {
    var playerResultDescription = ['', 'камень', 'ножницы', 'бумага'];
    return playerResultDescription[playerResultNumber];
}
function determineWinner(firstPlayer, secondPlayer){
    var combinedNumbers = +(String(firstPlayer) + String(secondPlayer));
    switch (combinedNumbers) {
        case 11:
        case 22:
        case 33:
            return 0;
            break;
        case 12:
        case 23:
        case 31:
            return 1;
            break;
        case 21:
        case 32:
        case 13:
            return 2;
            break;
    }
}
function printResult(winnerNumber) {
     var gameResult;
    switch(winnerNumber) {
        case 0:
            gameResult = "Ничья";
            break;
        case 1:
            gameResult = "Выиграл первый игрок";
            break;
        case 2:
            gameResult = "Выиграл второй игрок";
            break;
    }
    resultGame.innerHTML = gameResult;
}
function runGame() {
    var firstPlayerChoice = getPlayerResult();
    var secondPlayerChoice = getPlayerResult();
    player1.innerHTML = getNameById(firstPlayerChoice);
    player2.innerHTML = getNameById(secondPlayerChoice);
    printResult(determineWinner(firstPlayerChoice, secondPlayerChoice));
}
btn.addEventListener("click", runGame);