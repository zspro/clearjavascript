(function(){
    var add_slide = document.querySelector('#add-slide'),
        save_slides = document.querySelector('#save_slides'),
        slides_output = document.querySelector('#result'),
        slides_output_counter = document.querySelector('#count'),
        slide_counter = 0,
        successNotice = document.querySelector(".bg-success"),
        data_result = null;

    function createNewArray(data) {
        var modified = [];
        data.forEach(function(item, index){
            modified.push({
                url : item.url,
                name : item.name,
                params : item.params,
                description : item.description,
                date : item.date
            });
        })
        return modified;
    }

    function getModifiedArray(data) {
        return data.map(function(item){
            return {
                url:'http://'+ item.url,
                name:capitalizeFirstLetter(item.name.toLowerCase()),
                params:item.params.status+'=>'+item.params.progress,
                description:item.description.substring(0, 15) + '...',
                date:timestampToDate(item.date),
            }
        })
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function timestampToDate(timestamp) {
        var tmpDate = new Date(timestamp),
            year = tmpDate.getFullYear(),
            month = tmpDate.getMonth() + 1,
            day = tmpDate.getDate(),
            hours = tmpDate.getHours(),
            minutes = tmpDate.getMinutes();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }
        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        return year +'/'+ month+'/'+day +' '+ hours +':'+ minutes;
    }

    function addSlideItem(item, output){
        var resultHTML = "";
        var itemTemplate = `<div class="col-sm-3 col-xs-6 item" id="data_id_${slide_counter}">\
                            <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">${item.name}</div>\
                                <div class="text-muted">${item.description}</div>\
                                <div class="text-muted">${item.params}</div>\
                                <div class="text-muted">${item.date}</div>\
                                <input type="button" class="btn btn-danger remove_slide" value="remove" />
                            </div>\
                        </div>`;

        output.innerHTML += itemTemplate;
    }

    function removeSlide(event){
        var target = event.target,
            parent = target.closest('.item');
        parent.remove();
        slide_counter--;
        slides_output_counter.innerHTML = slide_counter;
    }

    function getAjaxResult(data){
        data_result = getModifiedArray(createNewArray(data));
    }

    function addingSlidesProcess(){
        if(slide_counter<data_result.length){
            addSlideItem(data_result[slide_counter], slides_output);
            slides_output_counter.innerHTML = slide_counter+1;
            slide_counter++;
        } else {
            alert('No more elements!');
        }
    }

    function checkIfDataReceived(){
        if(data_result==null){
            $.get( "js/data.json", function( data ) {
                getAjaxResult(data);
                addingSlidesProcess();
            });
        } else {
            addingSlidesProcess();
        }
    }

    function showSuccessMsg(){
       successNotice.classList.remove('hidden');
    }

    function saveSlides(){
        var result = {item: data_result };
        $.post( "api/save", result, function( data ) {
			if(data == 'ok') {
                showSuccessMsg();
            }
	   })
    }

    add_slide.addEventListener('click', checkIfDataReceived);
    slides_output.addEventListener('click', removeSlide);
    save_slides.addEventListener('click', saveSlides);

}())
