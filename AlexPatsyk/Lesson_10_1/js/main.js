function Validator(form){
    var DOMElements = null,
		count = 1;

    setFormData(form);

    function showErrorMsg(){
        DOMElements.errorBaloon.classList.remove('hidden');
    }

    function hideErrorMsg(){
        DOMElements.errorBaloon.classList.add('hidden');
    }

    function hasClass( elem, className ) {
        return elem.classList.contains(className);
    }

    function toggleHidden(elem){
        if(hasClass(elem, 'hidden')){
            elem.classList.remove('hidden');
        } else {
            elem.classList.add('hidden');
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function changePassToStar(str) {
		for (var i = 0; i < str.length; i++) {
			str = str.replace(str[i], '*')
		}
		return str;
	}

    function addNewTableLine() {
		var cell = null;
            row = DOMElements.tableContent.insertRow(DOMElements.tableContent.rows.length),
            arr = {
                id:count,
                name:DOMElements.name.value,
                email:DOMElements.email.value,
                password:DOMElements.password.value
            };

        cell = row.insertCell(0);
        cell.innerHTML = arr.id;
        cell = row.insertCell(1);
        cell.innerHTML = arr.name;
        cell = row.insertCell(2);
        cell.innerHTML = arr.email;
        cell = row.insertCell(3);
        cell.innerHTML = `<span class="stars">${changePassToStar(arr.password)}</span>
				          <span class="letters hidden">${arr.password}</span>
				          <button class="show-pass">Show Pass</button>`;
	}

    function changePassView(event){
        var target = event.target,
			targetClass = target.getAttribute('class'),
			stars = target.closest('td').querySelector('.stars'),
			letters = target.closest('td').querySelector('.letters');

        if(target.tagName == 'BUTTON'){
            if (targetClass == 'show-pass') {
                target.innerHTML = 'Hide Pass';
                target.className = 'hide-pass';
                stars.classList.add('hidden');
                letters.classList.remove('hidden');
            } else {
                target.innerHTML = 'Show Pass';
                target.className = 'show-pass';
                stars.classList.remove('hidden');
                letters.classList.add('hidden');
            }
        }
	}

    function validate(e) {
        e.preventDefault();
		if(DOMElements.name.value
			&& validateEmail(DOMElements.email.value)
			&& DOMElements.password.value) {

            addNewTableLine(count);
            toggleHidden(DOMElements.formBlock);
            toggleHidden(DOMElements.tableBlock);
            hideErrorMsg();
            toggleHidden(DOMElements.succesBaloon);
            count++;

		} else {
			showErrorMsg();
		}
	}

    function reset(){
        DOMElements.name.value = "";
        DOMElements.email.value = "";
        DOMElements.password.value = "";
        toggleHidden(DOMElements.tableBlock);
        toggleHidden(DOMElements.succesBaloon);
        toggleHidden(DOMElements.formBlock);
    }



    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate.bind(this));
        DOMElements.resetBtn.addEventListener("click", reset.bind(this));
        DOMElements.tableContent.addEventListener("click", changePassView);
	}

    function setFormData(form){
        DOMElements = form;
    }

    this.initValidator = function(form){
        initListeners.call(this);
    }

}

var validator = new Validator({
	name 	: document.querySelector("#inputName"),
	email 	: document.querySelector("#inputEmail"),
	password : document.querySelector("#inputPassword"),
	tableContent : document.querySelector("#table-content"),
	submitBtn: document.querySelector(".form-submit"),
	resetBtn: document.querySelector(".table-reset"),
    passButton: document.getElementById("form-result"),
    succesBaloon : document.querySelector(".bg-success"),
    errorBaloon : document.querySelector(".bg-danger"),
    tableBlock : document.querySelector("#form-result"),
    formBlock : document.querySelector("#form-block")

});
validator.initValidator();

