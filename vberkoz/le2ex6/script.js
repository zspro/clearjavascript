//Lesson 2, exercise 6 homework
//
//Declare a variable and assign it a meaning.
//Convert it alternately in each of the types.

var var1 = 0;

var1 = String(var1);
console.log("To string: " + var1);

var1 = Number(var1);
console.log("To number: " + var1);

var1 = Boolean(var1);
console.log("To boolean: " + var1);