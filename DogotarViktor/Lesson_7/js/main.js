
function editName(name){
    return name[0].toUpperCase() + name.slice(1).toLowerCase(); 
}
function editUrl(url){
    return "http://"+url;
}
function editDescriprion(description){
    return description.slice(0, 15) + "...";
}
function editParams(params){
    return params.status + "=>" + params.progress;
}


function editDate(date){
    var tmpDate = new Date(date);
    function getCorrectDate(val){
        if(val < 10){
            return "0" + val; 
        } else {return val;}
    }
    return tmpDate.getFullYear() + "/" +
           getCorrectDate(tmpDate.getMonth() + 1)   + "/"  +
           getCorrectDate(tmpDate.getDate()) + " " +
           getCorrectDate(tmpDate.getHours()) + ":" +
           getCorrectDate(tmpDate.getMinutes());
}


function transformNewArray(newArray){
    return newArray.map(function(item, index){
        return {
            name: editName(item.name),
            url: editUrl(item.url),
            description: editDescriprion(item.description),
            date: editDate(item.date),
            params: editParams(item.params)
        };
    })
   
}


function insertWithReplaceMethod(item, firstLine) {
    var resultHTML = "";
    var itemTemplate = '<div class="col-sm-3 col-xs-6">\
                <img src="$url" alt="$name" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">$name</div>\
                    <div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
                    <div class="text-muted">$date</div>\
                </div>\
            </div>';

    resultHTML = itemTemplate
        .replace(/\$name/gi, item.name)
        .replace("$url", item.url)
        .replace("$params", item.params)
        .replace("$description", item.description)
        .replace("$date", item.date);

    firstLine.innerHTML += resultHTML;

}

function insertWithInterpolationMethod(item, secondLine) {
    var itemTemplate = `<div class="col-sm-3 col-xs-6">\
                <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">${item.name}</div>\
                    <div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
                    <div class="text-muted">${item.date}</div>\
                </div>\
            </div>`;
    secondLine.innerHTML += itemTemplate;
}

function insertWithCreateElemMethod(item, thirdLine) {
    var elemDiv = document.createElement("div");
    elemDiv.className = "col-sm-3 col-xs-6";
    thirdLine.appendChild(elemDiv);

    var elemImg = document.createElement("img");
    elemImg.src = item.url;
    elemImg.className = "img-thumbnail";
    elemImg.alt = item.name;
    elemDiv.appendChild(elemImg);

    var divDescription = document.createElement('div');
    divDescription.className = "info-wrapper";
    elemDiv.appendChild(divDescription);

    var divItems = document.createElement('div');
    divItems.className = "text-muted";
    divDescription.appendChild(divItems);
    divItems.innerHTML = item.name;

    function cloneDiv(itemProperty) {
        var newEl = divItems.cloneNode(true);
        newEl.innerHTML = itemProperty;
        divDescription.appendChild(newEl);
    }
    cloneDiv(item.description);
    cloneDiv(item.params);
    cloneDiv(item.date);

}



(function transform() {    
    var firstLine = document.querySelector('#first-line'),
        secondLine = document.querySelector('#second-line'),
        thirdLine = document.getElementById('third-line'),
        convertedArray = transformNewArray(data);

        convertedArray.forEach(function(item, index) {
        if (index <= 2) {
            insertWithReplaceMethod(convertedArray[index], firstLine);
        } else if (index <= 5) {
            insertWithInterpolationMethod(convertedArray[index], secondLine);
        } else if (index < 9) {
            insertWithCreateElemMethod(convertedArray[index], thirdLine);
        }
    }); 
     
})();



