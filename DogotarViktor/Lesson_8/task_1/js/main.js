var gettext_area = document.querySelector('#gettext'),
	settext_area = document.querySelector('#settext'),
	count_area = document.querySelector('#count-letters'),
	reset_button = document.querySelector('#reset');
	
function clearTextArea(){
    gettext_area.value = '';
    gettext_area.disabled = false;
    settext_area.value = '';
    count_area.innerHTML = '0';
    count_area.style.color = 'black';
}

function showNumberCharacters(){
    var data = gettext_area.value,
        count = gettext_area.value.length;
	if(count>=200) {
		count_area.innerHTML = count;
		count_area.style.color = 'red';
        count_area.style.fontWeight = 'bold';
		gettext_area.disabled = true;
	} else {
		count_area.innerHTML = count;
		settext_area.value = data;
	}
}

gettext_area.addEventListener('keydown', showNumberCharacters);
reset_button.addEventListener('click', clearTextArea);

