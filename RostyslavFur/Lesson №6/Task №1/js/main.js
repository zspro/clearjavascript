var btn = document.getElementById("play");
function createData (oldData, count){
	var newList = [];
	oldData.forEach(function(item,index){
		if(count > index){
			newList.push({
				url: item.url,
				name: item.name,
				description: item.description,
				date: item.date,
				params: item.params
			});
		}
	});
	return newList;
}
var modifyName = (name) => {
	return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
};
var modifyUrl = (url) => {
	return 'http://' + url; 
};
var modifyDescript = (descript) => {
	return descript.substr(0, 15) + '...';
}; 
var modifyDate = (date) => {
        var tmpDate = new Date(date);
        var fixedDate = {
			year:    tmpDate.getFullYear(),
            month:   tmpDate.getMonth()+1,
            day:     tmpDate.getDate(),
            hours:   tmpDate.getHours(), 
            minutes: tmpDate.getMinutes(),
		};
		for (var key in fixedDate){
			fixedDate[key] = fixedDate[key] < 10 ? '0' + fixedDate[key] : fixedDate[key];
		}
		return fixedDate.year + '/' + fixedDate.month + '/' + fixedDate.day + ' ' + fixedDate.hours + ':' + fixedDate.minutes;
};
var modifyParams = (params) => {
	return params.status + ' => ' + params.progress;
};
var modifyData = (newData) => {
	return newData.map(function(item,index){
      return{
		name: modifyName(item.name),
        url: modifyUrl(item.url),
		description: modifyDescript(item.description),
		date: modifyDate(item.date),
		params: modifyParams(item.params)
      };
	});
};
var display = (text) => {
	console.log(text);
}; 
var transform = () => {
   var number = 7;
   var newData = createData(data, number);   
   var midifiedData = modifyData(newData);
   display(midifiedData); 
};
btn.addEventListener("click", transform);