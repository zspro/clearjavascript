var result           = document.querySelector('#result');
var addButton        = document.querySelector('#add');
var countElem        = document.querySelector('#count');
var visibleCount     = 0;
var arrayCount       = 0;
var transformedArray = transformArray(data);

function capitalizeFirstLetter(s) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}

function cutString (s){
    return s.slice(0, 15) + "...";
}

function formatDate(date){
    var tmpDate = new Date(date);
    var fixDate = {
        year    : tmpDate.getFullYear(),
        month   : tmpDate.getMonth() + 1,
        day     : tmpDate.getDate(),
        hours   : tmpDate.getHours(),
        minutes : tmpDate.getMinutes()
    };
    for (var k in fixDate) {
        fixDate[k] = fixDate[k] < 10  ? ('0'+ fixDate[k]) : fixDate[k];
    }
    return fixDate.year + "/" + fixDate.month + "/" + fixDate.day + " " +  fixDate.hours + ":" + fixDate.minutes;
}

function transformArray (arr){
    return arr.map(function(item) {
        return {
            id         : item.id,
            url        : "http://" + item.url,
            name       : capitalizeFirstLetter(item.name),
            params     : item.params.status + "=>" + item.params.progress,
            description: cutString(item.description),
            date       : formatDate(item.date)
        }
    });
}

function print(tmp, elem){
    elem.innerHTML += tmp;
}

function interpolateItems(item){
    var itemTemplate = `<div id="item-${item.id}"  class="col-sm-3 col-xs-6">\
        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
            <div class="info-wrapper">\
                <div class="text-muted">${item.name}</div>\
                <div class="text-muted">${item.description}</div>\
                <div class="text-muted">${item.params}</div>\
                <div class="text-muted">${item.date}</div>\
            </div>\
            <button onclick="removeItem(event)" class="btn btn-danger">Удалить</button>\
        </div>`;
    var result = "";
    result += itemTemplate;
    return result;
}

function buildGallery(arr){
    var galleryArray = [];
    arr.forEach(function(item) {
        galleryArray.push(interpolateItems(item));
    });
    return galleryArray;
}

function count(){
    countElem.innerHTML = visibleCount;
}

function showMsg(){
    alert("Изображений больше нет");
}

function addItem (arr, elem){
    if(visibleCount < arr.length){
        print(arr[arrayCount], elem);
        visibleCount++;
    }else{
        showMsg();
    }
    arrayCount < arr.length-1 ? arrayCount++ : arrayCount = 0;
}

function removeItem (event){
    var target = event.target;
    target.closest("div").remove();
    visibleCount--;
    count();
}

addButton.addEventListener("click", runGallery);

function runGallery() {
    addItem(buildGallery(transformedArray), result);
    count();
}

// Counter

var getText = document.getElementById('gettext');
var setText = document.getElementById('settext');
var countCharElem = document.getElementById('count-letters');
var reset = document.getElementById('reset');

function dublicateChar(){
    setText.value = getText.value;
}

function countChar(){
    return getText.value.length;
}

function printChar(number, elem){
    elem.innerHTML = number;
}

function disableCharEnter(number, limit){
    if (number >= limit){
        getText.setAttribute("onkeypress", "return false");
        getText.value = getText.value.substring(0, limit);
        showErrorNotice(true);
    }else {
        getText.setAttribute("onkeypress", "return true");
        showErrorNotice(false);
    }
}

function showErrorNotice(status){
    countCharElem.className = status ? "error" :  '';
}

getText.addEventListener("keyup", runCounter);

function runCounter(){
    var charNumber = countChar();
    disableCharEnter(charNumber, 200);
    dublicateChar();
    printChar(charNumber, countCharElem);
}

reset.addEventListener("click", function(){
    getText.value = "";
    setText.value = "";
    printChar(countChar(), countCharElem);
    showErrorNotice(false);
});

// Tabs

var navbar = document.getElementById('myNavbar');

navbar.addEventListener("click", runTabSwitcher);

function runTabSwitcher(event){
    var target = event.target;
    var tabName = target.getAttribute("data-target");
    document.querySelector(".activeTab").className = "";
    document.getElementById(tabName).className = "activeTab";
}





